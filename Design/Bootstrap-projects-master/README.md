<p align="center">
  <a href="https://getbootstrap.com/">
    <img src="https://getbootstrap.com/docs/5.0/assets/brand/bootstrap-logo-shadow.png" alt="Bootstrap logo" width="200" height="165">
  </a>

### Quick start
Looking to quickly add Bootstrap to your project? Use jsDelivr, a free open source CDN. Using a package manager or need to download the source files? Head to the downloads page.

### CSS
Copy-paste the stylesheet ```<link>``` into your ```<head>``` before all other stylesheets to load our CSS.

 ```<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">```


### JS
Many of our components require the use of JavaScript to function. Specifically, they require our own JavaScript plugins and Popper. Place one of the following <script>s near the end of your pages, right before the closing </body> tag, to enable them.

 ```<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script> ```

### Separate
If you decide to go with the separate scripts solution, Popper must come first (if you’re using tooltips or popovers), and then our JavaScript plugins.

 ``` <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>
 ```
 
 ## Startet template
 Be sure to have your pages set up with the latest design and development standards. That means using an HTML5 doctype and including a viewport meta tag for proper responsive behaviors. Put it all together and your pages should look like this:
 
  ``` <!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>

    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.min.js" integrity="sha384-lpyLfhYuitXl2zRZ5Bn2fqnhNAKOAaM/0Kr9laMspuaMiZfGmfwRNFh8HlMy49eQ" crossorigin="anonymous"></script>
    -->
  </body>
</html>
 ```
## Contents
Discover what’s included in Bootstrap, including our precompiled and source code flavors.

 ``` bootstrap/
+-- css/
¦   +-- bootstrap-grid.css
¦   +-- bootstrap-grid.css.map
¦   +-- bootstrap-grid.min.css
¦   +-- bootstrap-grid.min.css.map
¦   +-- bootstrap-grid.rtl.css
¦   +-- bootstrap-grid.rtl.css.map
¦   +-- bootstrap-grid.rtl.min.css
¦   +-- bootstrap-grid.rtl.min.css.map
¦   +-- bootstrap-reboot.css
¦   +-- bootstrap-reboot.css.map
¦   +-- bootstrap-reboot.min.css
¦   +-- bootstrap-reboot.min.css.map
¦   +-- bootstrap-reboot.rtl.css
¦   +-- bootstrap-reboot.rtl.css.map
¦   +-- bootstrap-reboot.rtl.min.css
¦   +-- bootstrap-reboot.rtl.min.css.map
¦   +-- bootstrap-utilities.css
¦   +-- bootstrap-utilities.css.map
¦   +-- bootstrap-utilities.min.css
¦   +-- bootstrap-utilities.min.css.map
¦   +-- bootstrap-utilities.rtl.css
¦   +-- bootstrap-utilities.rtl.css.map
¦   +-- bootstrap-utilities.rtl.min.css
¦   +-- bootstrap-utilities.rtl.min.css.map
¦   +-- bootstrap.css
¦   +-- bootstrap.css.map
¦   +-- bootstrap.min.css
¦   +-- bootstrap.min.css.map
¦   +-- bootstrap.rtl.css
¦   +-- bootstrap.rtl.css.map
¦   +-- bootstrap.rtl.min.css
¦   +-- bootstrap.rtl.min.css.map
+-- js/
    +-- bootstrap.bundle.js
    +-- bootstrap.bundle.js.map
    +-- bootstrap.bundle.min.js
    +-- bootstrap.bundle.min.js.map
    +-- bootstrap.esm.js
    +-- bootstrap.esm.js.map
    +-- bootstrap.esm.min.js
    +-- bootstrap.esm.min.js.map
    +-- bootstrap.js
    +-- bootstrap.js.map
    +-- bootstrap.min.js
    +-- bootstrap.min.js.map
```