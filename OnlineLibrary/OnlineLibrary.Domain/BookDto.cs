﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.Domain
{
    public class BookDto
    {
        public int? Id { get; set; }
        public string Caption { get; set; }
        public int PublicationYear { get; set; }
        public string Image { get; set; }

        public decimal? Rate { get; set; }
        public DateTime? DeletedOnUtc { get; set; }

        public BookDto() { }
        
        
    }
}
