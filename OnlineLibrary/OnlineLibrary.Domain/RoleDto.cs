﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Domain
{
    public class RoleDto
    {
        public int Id { get; set; }
        public string Caption { get; set; }
        public string Description { get; set; }
    }
}
