﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Domain
{
    public class UserDto
    {
        public int Id { get; set; }

        public string Username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Password { get; set; }
        public System.DateTime AddedOnUtc { get; set; }
        public Nullable<System.DateTime> DeletedOnUtc { get; set; }
        public string Image { get; set; }
        public string UserToken { get; set; }

        public int RoleId { get; set; }
    }
}
