﻿using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.CRUD
{
    public class Roles
    {
        public static List<RoleDto> GetAll()
        {
            using (var dc = new OnlineLibraryDBEntities())
            {
                var books = (from role in dc.Roles
                             
                             select new RoleDto()
                             {
                                 Id = role.Id,
                                 Caption = role.Caption,
                                Description =  role.Description
                             }).ToList();
                return books;
            }
        }

        public static List<RoleDto> GetAllByUsername(string username)
        {
            using (var dc = new OnlineLibraryDBEntities())
            {
                var books = (from role in dc.Roles
                             join user in dc.Users on role.Id equals user.RoleId
                             where user.Username == username
                             select new RoleDto()
                             {
                                 Id = role.Id,
                                 Caption = role.Caption,
                                 Description = role.Description
                             }).ToList();
                return books;
            }
        }

    }
}
