﻿using OnlineLibrary.Data.Converters;
using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.CRUD
{
    public static class Role
    {

        public static RoleDto GetBookByName(string name)
        {
            using (var dc = new OnlineLibraryDBEntities())
            {
                var bookDto = (from role in dc.Roles
                               where role.Caption == name
                               select new RoleDto()
                               {
                                   Id = role.Id,
                                   Caption = role.Caption,
                                  Description =  role.Description

                               }).FirstOrDefault();

                return bookDto;
            }
        }





        public static int Insert(RoleDto dto)
        {
            using (var dc = new OnlineLibraryDBEntities()) //kreiranje instance Data contexta, context sadrzi liste objekata mapiranih iz tabela u bazi (svaka tabela je jedna lista)
            {
                var data = RoleConverter.toData(dto); //konvertovanje dto objekta u objekat koji je generisao Entity framework, objekat tzv POCO klase

                dc.Roles.Add(data); //kao sto je napomenuto dc se sastoji od lista, jedna od tih lista je Books, ona sluzi za cuvanje knjiga, svaka knjiga dodata ili obrisana u ovoj listi reflektuje se u bazi pozivanjem SaveChanges ispod

                dc.SaveChanges(); //cuva izmene napravljene u context-u

                return data.Id; //id se popuni nakon poziva u SaveChanges tada se upise objekat i inkrementuje id. id je 0 u data dok se ne upise u context
            }
        }

    }
}
