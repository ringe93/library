﻿using OnlineLibrary.Data.Common;
using OnlineLibrary.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.CRUD
{
    public class Books
    {
        public static List<BookDto> GetAll()
        {
            using (var dc = new OnlineLibraryDBEntities())
            {
                var books = (from book in dc.Books
                            join rate in  dc.Rates  on book.Id equals rate.BookId  into bookRates
                            from bookRate in bookRates.DefaultIfEmpty()
                            select new BookDto()
                            {
                                Id = book.Id,
                                Caption = book.Caption,
                                PublicationYear = book.PublicationYear,
                                Image = book.Image,
                                Rate = bookRate.Mark,
                                DeletedOnUtc = book.DeletedOnUtc
                            }).ToList();
                return books;
            }
        }

        public static List<BookDto> GetTopRated(int number = Constants.Books.RatedBooks)
        {
            using (var dc = new OnlineLibraryDBEntities())
            {
                var topRated = (from book in dc.Books
                               join rate in dc.Rates
                               on book.Id equals rate.Id
                               orderby rate.Mark descending
                               select new BookDto()
                               {
                                   Id = book.Id,
                                   Caption = book.Caption,
                                   PublicationYear = book.PublicationYear,
                                   Image = book.Image,
                                   Rate = rate.Mark,
                                   DeletedOnUtc = book.DeletedOnUtc
                               }).Take(number).ToList();

               

                return topRated;
            }

        }
    }
}
