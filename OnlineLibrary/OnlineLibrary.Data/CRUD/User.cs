﻿using OnlineLibrary.Data.Converters;
using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.CRUD
{
    public static class User
    {
        public static int Insert(UserDto dto)
        {
            using (var dc = new OnlineLibraryDBEntities()) //kreiranje instance Data contexta, context sadrzi liste objekata mapiranih iz tabela u bazi (svaka tabela je jedna lista)
            {
                var data = UserConverter.toData(dto); //konvertovanje dto objekta u objekat koji je generisao Entity framework, objekat tzv POCO klase
                var existingUser = (from user in dc.Users
                               where user.Username == dto.Username select user).FirstOrDefault();                                      // data.Password = data.Password.Substring(0, 39);
                if (existingUser == null)
                {
                    dc.Users.Add(data); //kao sto je napomenuto dc se sastoji od lista, jedna od tih lista je Books, ona sluzi za cuvanje knjiga, svaka knjiga dodata ili obrisana u ovoj listi reflektuje se u bazi pozivanjem SaveChanges ispod

                    dc.SaveChanges(); //cuva izmene napravljene u context-u
                }
                return existingUser != null? existingUser.Id : data.Id; //id se popuni nakon poziva u SaveChanges tada se upise objekat i inkrementuje id. id je 0 u data dok se ne upise u context
            }
        }

        public static UserDto GetByUserName(string username)
        {
            using (var dc = new OnlineLibraryDBEntities()) //kreiranje instance Data contexta, context sadrzi liste objekata mapiranih iz tabela u bazi (svaka tabela je jedna lista)
            {
                var userDto = (from user in dc.Users
                               where user.Username == username
                               select new UserDto()
                               {
                                   Id = user.Id,
                                   Username = user.Username,
                                   AddedOnUtc  = user.AddedOnUtc,
                                   Firstname = user.Firstname,
                                   Lastname = user.Lastname,
                                   Password = user.Password,
                                   Image = user.Image,
                                   UserToken = user.UserToken,
                                   DeletedOnUtc = user.DeletedOnUtc,
                                   RoleId = (int)user.RoleId
                               }).FirstOrDefault();

                return userDto;
            }
        }

        public static UserDto GetByToken(string token)
        {
            using (var dc = new OnlineLibraryDBEntities()) //kreiranje instance Data contexta, context sadrzi liste objekata mapiranih iz tabela u bazi (svaka tabela je jedna lista)
            {
                var userDto = (from user in dc.Users
                               where user.UserToken == token
                               select new UserDto()
                               {
                                   Id = user.Id,
                                   UserToken = user.UserToken,
                                   Username = user.Username,
                                   AddedOnUtc = user.AddedOnUtc,
                                   Firstname = user.Firstname,
                                   Lastname = user.Lastname,
                                   Password = user.Password,
                                   Image = user.Image,
                                   DeletedOnUtc = user.DeletedOnUtc
                               }).FirstOrDefault();

                return userDto;
            }

        }
    }
}
