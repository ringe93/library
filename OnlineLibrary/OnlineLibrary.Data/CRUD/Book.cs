﻿using OnlineLibrary.Data.Converters;
using OnlineLibrary.Data.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.CRUD
{
    public static class Book
    {
        /// <summary>
        /// Ova metoda kao argument prima caption a vraca knjigu na osnovu tog naslova
        /// </summary>
        /// <param name="caption"></param>
        /// <returns></returns>
        public static BookDto GetBookByCaption(string caption)
        {
            using (var dc = new OnlineLibraryDBEntities())
            {
                //upit koji se kreira je slican SQl upitu samo sto rezultat book-a konvertujemo u BookDto i odmah ga setujemo podacima iz book
                var bookDto = (from book in dc.Books where book.Caption == caption select new BookDto()
                {
                    Id = book.Id,
                    Caption = book.Caption,
                    PublicationYear = book.PublicationYear,
                    Image = book.Image,
                    DeletedOnUtc = book.DeletedOnUtc
                }).FirstOrDefault(); //FirstOrDefault metoda uzima prvi objekat ako postoji, ako ne postoji ni jedan da zadovoljava uslov vrednost bookDto je null

                return bookDto;
            }
            
        }

        /// <summary>
        /// Ova metoda kao argument prima id i vraca knjigu na osnovu tog id-a
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static BookDto GetBookById(int id)
        {
            using(var dc = new OnlineLibraryDBEntities())
            {
                var bookDto = (from book in dc.Books
                               where book.Id == id
                               select new BookDto()
                               {
                                   Id = book.Id,
                                   Caption = book.Caption,
                                   PublicationYear = book.PublicationYear,
                                   Image = book.Image,
                                   DeletedOnUtc = book.DeletedOnUtc
                               }).FirstOrDefault();

                return bookDto;
            }
        }

        

        

        public static int Insert(BookDto dto)
        {
            using (var dc = new OnlineLibraryDBEntities()) //kreiranje instance Data contexta, context sadrzi liste objekata mapiranih iz tabela u bazi (svaka tabela je jedna lista)
            {
                var data = BookConverter.toData(dto); //konvertovanje dto objekta u objekat koji je generisao Entity framework, objekat tzv POCO klase

                dc.Books.Add(data); //kao sto je napomenuto dc se sastoji od lista, jedna od tih lista je Books, ona sluzi za cuvanje knjiga, svaka knjiga dodata ili obrisana u ovoj listi reflektuje se u bazi pozivanjem SaveChanges ispod

                dc.SaveChanges(); //cuva izmene napravljene u context-u
                    
                return data.Id; //id se popuni nakon poziva u SaveChanges tada se upise objekat i inkrementuje id. id je 0 u data dok se ne upise u context
            }
        }

        public static int Edit(BookDto dto)
        {
            using(var dc = new OnlineLibraryDBEntities())
            {
                var data = BookConverter.toData(dto);
                data.Id = (int)dto.Id;
                dc.Entry(data).State = EntityState.Modified; //naglasavamo Entity-u da je objekat "data", koji predstavlja entitet iz tabele, promenjen(altered) i da treba da se alter-uje taj entitet u tabeli
                dc.SaveChanges();
                return data.Id;
            }
        }

        public static void Delete(BookDto dto)
        {
            using(var dc = new OnlineLibraryDBEntities())
            {
                var data = BookConverter.toData(dto);
                data.Id = (int)dto.Id;
                data.DeletedOnUtc = DateTime.Now;
                dc.Entry(data).State = EntityState.Modified;
                dc.SaveChanges();
            }
        }
    }
}
