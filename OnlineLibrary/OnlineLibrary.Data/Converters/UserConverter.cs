﻿using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.Converters
{
    public static class UserConverter
    {

        public static Data.Users toData(UserDto dto)
        {
            return dto == null ? null : new Data.Users()
            {
                Firstname = dto.Firstname,
                Lastname = dto.Lastname,
                AddedOnUtc = dto.AddedOnUtc,
                Password = dto.Password,
                Username = dto.Username,
                Image = dto.Image,
                UserToken = dto.UserToken,
                RoleId = dto.RoleId
            };
        }
    }
}
