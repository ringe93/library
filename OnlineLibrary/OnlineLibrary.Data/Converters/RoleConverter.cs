﻿using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.Converters
{
    public static class RoleConverter
    {
        public static Data.Roles toData(RoleDto dto)
        {
            return dto == null ? null : new Data.Roles()
            {
                Id = dto.Id,
                Caption = dto.Caption,
                Description = dto.Description               
            };
        }
    }
}
