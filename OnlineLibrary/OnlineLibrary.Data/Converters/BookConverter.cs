﻿using OnlineLibrary.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Data.Converters
{
    /// <summary>
    /// konverter sluzi za konvertovanje Dto objekata u objekte POCO klase
    /// </summary>
    public static class BookConverter
    {
        public static Data.Books toData(BookDto dto)
        {
            return new Data.Books()
            {
                Caption = dto.Caption,
                Image = dto.Image,
                PublicationYear = dto.PublicationYear,
                DeletedOnUtc = dto.DeletedOnUtc
            };
        }
    }
}
