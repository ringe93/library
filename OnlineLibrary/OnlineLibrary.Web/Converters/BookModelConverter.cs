﻿using OnlineLibrary.Data.Domain;
using OnlineLibrary.Web.Models;
using OnlineLibrary.Web.Models.Component;
using OnlineLibrary.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineLibrary.Web.Converters
{
    public static class BookModelConverter
    {
        public static BookModel ToModel(BookDto dto)
        {
            if(dto == null)
            {
                return null;
            }

            return new BookModel()
            {
                Id = dto.Id,
                Caption = dto.Caption,
                PublicationYear = dto.PublicationYear,
                Image = dto.Image,
                DeletedOnUtc = dto.DeletedOnUtc
            };
        }

        public static _BookBadge ToBadge(BookDto dto)
        {
            if (dto == null)
            {
                return null;
            }

            return new _BookBadge()
            {
                Id = (int)dto.Id,
                Title = dto.Caption,
                Image = dto.Image,
                Rate = dto.Rate
            };
        }

        public static List<_BookBadge> ToBadge(List<BookDto> dtos)
        {
            var model = new List<_BookBadge>();
 
            foreach (BookDto dto in dtos)
                model.Add(ToBadge(dto));
            return model;
        }

        public static BookDto ToDTO(BookModel bvm)
        {
            return new BookDto()
            {
                Id = bvm.Id,
                Caption = bvm.Caption,
                PublicationYear = bvm.PublicationYear,
                Image = bvm.Image,
                DeletedOnUtc = bvm.DeletedOnUtc
            };
        }

        public static BooksModel ToModel(IQueryable<BookDto> dtos)
        {
           BooksModel model = new BooksModel();
            foreach (BookDto dto in dtos)
                model.Books.Add(ToModel(dto));

            return model;
        }
    }
}