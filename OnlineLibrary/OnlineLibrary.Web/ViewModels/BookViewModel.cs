﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineLibrary.Web.ViewModels
{
    public class BookViewModel
    {
        public int? Id { get; set; }
        public string Caption { get; set; }
        public int PublicationYear { get; set; }
        public string Image { get; set; }
        public DateTime? DeletedOnUtc { get; set; }
    }
}