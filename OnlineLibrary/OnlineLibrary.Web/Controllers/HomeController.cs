﻿using OnlineLibrary.Data.Domain;
using OnlineLibrary.Service;
using OnlineLibrary.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using OnlineLibrary.Web.ViewModels;
using OnlineLibrary.Web.Converters;

namespace OnlineLibrary.Web.Controllers
{
    public class HomeController : Controller
    {

        public readonly IBookService BookService = new BookService();

        public ActionResult Index()
        {
            var booksDto = BookService.GetTopRated();

            var model = BookModelConverter.ToBadge(booksDto);
            return View(model);
          
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        
    }
}