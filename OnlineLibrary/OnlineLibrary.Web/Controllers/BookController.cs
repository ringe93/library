﻿using OnlineLibrary.Data.Domain;
using OnlineLibrary.Service;
using OnlineLibrary.Service.Interfaces;
using OnlineLibrary.Web.Converters;
using OnlineLibrary.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace OnlineLibrary.Web.Controllers
{
    public class BookController : Controller
    {
        public readonly IBookService BookService = new BookService();
        
        
        public ActionResult Books()
        {
            var booksDto =  BookService.GetAll();

            var model = BookModelConverter.ToBadge(booksDto);
            return View(model);
        }

        // GET: localhost/Home/Details/5
        public ActionResult Details(int? id) //parametar "id" moze imati null vrednost
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest); //na ovaj nacin handle-ujemo parametar "id" ukoliko ima vrednost null
            }
            var model = BookModelConverter.ToModel(BookService.GetBookById((int)id)); //dobavljamo knjigu iz baze sa tim id-em
            if (model == null || model.DeletedOnUtc != null)
            {
                return HttpNotFound(); //na ovaj nacin handle-ujemo ukoliko u bazi ne postoji knjiga sa tim id-em
            }

            return View(model); //prosledjivanje te knjige korespodentnom view-u
        }

        // GET: localhost/Home/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: localhost/Home/Create
        [HttpPost] //atribut koji kaze da ova metoda handle-uje samo HTTP POST zahteve
        [ValidateAntiForgeryToken] //atribut za prevenciju "falsifikovanja" zahteva
        public ActionResult Create(BookModel model)
        {
            if (ModelState.IsValid)
            {
                BookService.Insert(BookModelConverter.ToDTO(model));
                //return RedirectToAction(nameof(HomeController.Index), nameof(HomeController));
            }

            return View(model);
        }

        // GET: localhost/Home/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = BookModelConverter.ToModel(BookService.GetBookById((int)id));
            if (model == null || model.DeletedOnUtc != null)
            {
                return HttpNotFound();
            }

            return View(model);
        }

        // POST: localhost/Home/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(BookModel model)
        {
            if (ModelState.IsValid)
            {
                BookService.Edit(BookModelConverter.ToDTO(model));
                return RedirectToAction(nameof(HomeController.Index), nameof(HomeController));
            }

            return View(model);
        }

        // GET: localhost/Home/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var bvm = BookModelConverter.ToModel(BookService.GetBookById((int)id));
            if (bvm == null || bvm.DeletedOnUtc != null)
            {
                return HttpNotFound();
            }

            return View(bvm);
        }

        // POST: localhost/Home/Delete/5
        [HttpPost, ActionName("Delete")] //atribut ActionName govori da metoda pripada akciji "delete"
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            BookDto dto = BookService.GetBookById(id);
            BookService.Delete(dto);
            return RedirectToAction(nameof(HomeController.Index), nameof(HomeController));
        }

    }
}