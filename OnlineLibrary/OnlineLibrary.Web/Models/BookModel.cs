using System;
using System.ComponentModel.DataAnnotations;


public class BookModel {
    public int? Id { get; set; }
    [Required]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "Caption cannot be longer than 50 characters and less than 3 characters") ]
    public string Caption { get; set; }
    public int PublicationYear { get; set; }
    public string Image { get; set; }
    
    public decimal Rate { get; set; }

    public DateTime? DeletedOnUtc { get; set; }
    public BookModel() { }
}