﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using OnlineLibrary.Domain;
using OnlineLibrary.Service;
using OnlineLibrary.Service.Interfaces;

namespace OnlineLibrary.Web.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit https://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName { get { return FirstName + LastName; } }
        public string Password { get; set; }
        public System.DateTime AddedOnUtc { get; set; }
        public Nullable<System.DateTime> DeletedOnUtc { get; set; }
        public int RoleId {get;set;}
        public string Image { get; set; }

        
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            //this.SecurityStamp = Guid.NewGuid().ToString();

            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            userIdentity.AddClaim(new Claim("FirstName", this.FirstName));
            userIdentity.AddClaim(new Claim("LastName", this.LastName));
            userIdentity.AddClaim(new Claim("FullName", this.FullName));
            userIdentity.AddClaim(new Claim("Id", this.Id));
            return userIdentity;
        }
        public ApplicationUser() { }
        public ApplicationUser(UserDto dto) {
            
            FirstName = dto.Firstname;
            LastName = dto.Lastname;
            UserName = dto.Username;
            Email = dto.Username;
            Password = dto.Password;
            Image = dto.Image;
            AddedOnUtc = dto.AddedOnUtc;
            Id = dto.UserToken;
            RoleId = dto.RoleId;
            DeletedOnUtc = dto.DeletedOnUtc;
            SecurityStamp = Guid.NewGuid().ToString();
        }
        public UserDto AppUserToUserDto(ApplicationUser userApplication)
        {
            var userDto = new UserDto
            {                          
                Firstname = userApplication.FirstName,
                Lastname = userApplication.LastName,
                Username = userApplication.Email,
                Password = userApplication.Password,
                Image = userApplication.Image,
                UserToken = userApplication.Id,
                AddedOnUtc = userApplication.AddedOnUtc,
                DeletedOnUtc = userApplication.DeletedOnUtc,
                RoleId = userApplication.RoleId
            };



            return userDto;
        }
    }
    public class UserStore : IUserStore<ApplicationUser>, IUserEmailStore<ApplicationUser>, IUserLockoutStore<ApplicationUser, string>, IUserPasswordStore<ApplicationUser, string>, IUserTwoFactorStore<ApplicationUser, string>, IUserTokenProvider<ApplicationUser, string>, IUserRoleStore<ApplicationUser>, IPasswordHasher, IUserPhoneNumberStore<ApplicationUser>
    {
        private readonly UserService userService = new UserService();
        private readonly IRoleService roleService = new RoleService();
        public Task AddToRoleAsync(ApplicationUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task CreateAsync(ApplicationUser applicationUser)
        {

            var userDto = applicationUser.AppUserToUserDto(applicationUser);
            if(userDto != null)
                userService.Insert(userDto);

            return Task.FromResult(0);


        }

        public Task DeleteAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            //GC.SuppressFinalize(this);
        }

        public Task<ApplicationUser> FindByEmailAsync(string email)
        {
            var userDto = userService.GetByUserName(email);

            var appUser = userDto == null ? null : new ApplicationUser(userDto);

            return Task.FromResult(appUser);
        }

        public Task<ApplicationUser> FindByIdAsync(string token)
        {
            var userDto = userService.GetByToken(token);

            var appUser = userDto == null ? null : new ApplicationUser(userDto);

            return Task.FromResult(appUser);
            
        }

        public Task<ApplicationUser> FindByNameAsync(string userName)
        {
            var userDto =  userService.GetByUserName(userName);

            var appUser = userDto == null ? null: new ApplicationUser(userDto);
            return Task.FromResult(appUser);
        }

        public Task<string> GenerateAsync(string purpose, UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<int> GetAccessFailedCountAsync(ApplicationUser user)
        {
            return Task.FromResult(0);
        }

        public Task<string> GetEmailAsync(ApplicationUser user)
        {
            UserDto userDto = null;

            userDto = userService.GetByUserName(user.UserName);
            var appUser = userDto == null ? null :new ApplicationUser(userDto);

            if (userDto == null)
            {                
                userService.Insert(user.AppUserToUserDto(user));
                return Task.FromResult(user.Email);
            }
            return Task.FromResult(user.UserName);
        }

        public Task<bool> GetEmailConfirmedAsync(ApplicationUser user)
        {
            return Task.FromResult(true);
        }

        public Task<bool> GetLockoutEnabledAsync(ApplicationUser user)
        {
            return Task.FromResult(false);
        }

        public Task<DateTimeOffset> GetLockoutEndDateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<string> GetPasswordHashAsync(ApplicationUser user)
        {
            return Task.FromResult(user.Password);
        }

        public Task<string> GetPhoneNumberAsync(ApplicationUser user)
        {
            return Task.FromResult(string.Empty);
        }

        public Task<bool> GetPhoneNumberConfirmedAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            var roles = roleService.GetAllByUsername(user.UserName);

            IList<string> userRoles = roles?.Select(role =>  role.Caption ).ToList();

            return Task.FromResult(userRoles);
        }

        public Task<bool> GetTwoFactorEnabledAsync(ApplicationUser user)
        {
            return Task.FromResult(false);
        }

        public string HashPassword(string password)
        {
            throw new NotImplementedException();
        }

        public Task<bool> HasPasswordAsync(ApplicationUser user)
        {
            var bytes = Encoding.UTF8.GetBytes(user.Password);
            var base64 = Convert.ToBase64String(bytes);
            return Task.FromResult(bool.Parse(base64));
        }

        public Task<int> IncrementAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsInRoleAsync(ApplicationUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task<bool> IsValidProviderForUserAsync(UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            if (manager == null)
            {
                throw new ArgumentNullException();
            }
            return Task.FromResult(manager.SupportsUserPassword);
        }

        public Task NotifyAsync(string token, UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task RemoveFromRoleAsync(ApplicationUser user, string roleName)
        {
            throw new NotImplementedException();
        }

        public Task ResetAccessFailedCountAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailAsync(ApplicationUser user, string email)
        {
            throw new NotImplementedException();
        }

        public Task SetEmailConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task SetLockoutEnabledAsync(ApplicationUser user, bool enabled)
        {
            return Task.FromResult<object>(null);
        }

        public Task SetLockoutEndDateAsync(ApplicationUser user, DateTimeOffset lockoutEnd)
        {
            throw new NotImplementedException();
        }

        public Task SetPasswordHashAsync(ApplicationUser user, string passwordHash)
        {
            user.Password = passwordHash;
            return Task.FromResult<object>(null);
        }

        public Task SetPhoneNumberAsync(ApplicationUser user, string phoneNumber)
        {
            throw new NotImplementedException();
        }

        public Task SetPhoneNumberConfirmedAsync(ApplicationUser user, bool confirmed)
        {
            throw new NotImplementedException();
        }

        public Task SetTwoFactorEnabledAsync(ApplicationUser user, bool enabled)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<ApplicationUser, string> manager, ApplicationUser user)
        {
            throw new NotImplementedException();
        }

        public PasswordVerificationResult VerifyHashedPassword(string hashedPassword, string providedPassword)
        {
            if (hashedPassword == providedPassword)
                return PasswordVerificationResult.Success;
            return PasswordVerificationResult.Failed;
        }
    }
        public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public System.Data.Entity.DbSet<OnlineLibrary.Data.Domain.BookDto> BookDtoes { get; set; }

        public System.Data.Entity.DbSet<OnlineLibrary.Web.ViewModels.BookViewModel> BookViewModels { get; set; }

        public System.Data.Entity.DbSet<BookModel> BookModels { get; set; }

        public System.Data.Entity.DbSet<OnlineLibrary.Web.Models.Component._BookBadge> _BookBadge { get; set; }
    }
}