﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OnlineLibrary.Web.Models
{
    public class BooksModel
    {
        public List<BookModel> Books { get; set; }

        public BooksModel()
        {
            Books = new List<BookModel>();
        }
    }
}