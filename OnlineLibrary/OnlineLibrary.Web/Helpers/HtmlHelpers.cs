﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace OnlineLibrary.Web.Helpers
{
    public static class HtmlHelpers
    {
		private const string ComponentPartialsFolder = "Components";
		public static MvcHtmlString Component(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
		{
			return htmlHelper.Partial($"{ComponentPartialsFolder}/{partialViewName}", viewData);
		}
		public static MvcHtmlString Component(this HtmlHelper htmlHelper, string partialViewName, object model)
		{
			return htmlHelper.Partial($"{ComponentPartialsFolder}/{partialViewName}", model);
		}
		public static MvcHtmlString Component(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData)
		{
			return htmlHelper.Partial($"{ComponentPartialsFolder}/{partialViewName}", model, viewData);
		}

		public static void RenderComponent(this HtmlHelper htmlHelper, string partialViewName)
		{
			htmlHelper.RenderPartial($"{ComponentPartialsFolder}/{partialViewName}");
		}
		public static void RenderComponent(this HtmlHelper htmlHelper, string partialViewName, ViewDataDictionary viewData)
		{
			htmlHelper.RenderPartial($"{ComponentPartialsFolder}/{partialViewName}", viewData);
		}
		public static void RenderComponent(this HtmlHelper htmlHelper, string partialViewName, object model)
		{
			htmlHelper.RenderPartial($"{ComponentPartialsFolder}/{partialViewName}", model);
		}
		public static void RenderComponent(this HtmlHelper htmlHelper, string partialViewName, object model, ViewDataDictionary viewData)
		{
			htmlHelper.RenderPartial($"{ComponentPartialsFolder}/{partialViewName}", model, viewData);
		}
        public static MvcHtmlString Image(this HtmlHelper htmlHelper, string src, string id = null, string alt = null, string height = null, string width = null, string className = null)
        {
            // Below line is used for generate new tag with img  
            var builder = new TagBuilder("img");

            if (!string.IsNullOrEmpty(id))
                builder.MergeAttribute("id", id);

			if (!string.IsNullOrEmpty(src))
				builder.MergeAttribute("src", src);

            if (!string.IsNullOrEmpty(alt))
                builder.MergeAttribute("alt", alt);

            if (!string.IsNullOrEmpty(height))
                builder.MergeAttribute("height", height);

            if (!string.IsNullOrEmpty(width))
                builder.MergeAttribute("width", width);

			if (!string.IsNullOrEmpty(className))
				builder.MergeAttribute("class", className);

			// this method will return MVChtmlstring and Create method will render as selfclosing tag.  

			return MvcHtmlString.Create(builder.ToString(TagRenderMode.SelfClosing));
        }

		public static MvcHtmlString Link(this HtmlHelper htmlHelper, string actionName, string controllerName, string linkText,  string className = null, bool submit = false)
        {
			var builder = new TagBuilder("a");
			
			var url = string.Format("../{0}/{1}",
									 controllerName, actionName);
			builder.MergeAttribute("href", url);

			if (!string.IsNullOrEmpty(className))
				builder.MergeAttribute("class", className);
			builder.InnerHtml = linkText;

			return MvcHtmlString.Create(builder.ToString(TagRenderMode.Normal));
		}
	
	}
}