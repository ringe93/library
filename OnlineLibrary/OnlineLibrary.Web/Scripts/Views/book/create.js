$("input:file").change(function (event) {
  var fileName = $(this).val();


  var reader = new FileReader();

  reader.onload = function (e) {
    let image = e.target.result;
    console.log(image);
      $('.book-img').attr('src', image);
      $('#Image').val(image);
  }
  reader.readAsDataURL(this.files[0]);
  var fileExtensionAt = fileName.lastIndexOf(".") + 1;
  var fileExtension = fileName.substring(fileExtensionAt, fileName.length);
  if (fileName != "" && fileExtension == "png" || fileExtension == "jpeg" || fileExtension == "jpg") {
      var from = fileName.lastIndexOf("\\") + 1;
      var to = fileName.length;
      if (to < 12) {
        fileName = fileName.substring(from, to);
      }
      else {
        fileName = fileName.substring(from, from + 12) + " ...";
      }
  }
  else {
      fileName = "Choose file";
  }

 

});
