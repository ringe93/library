﻿function onload(event) {

    var myDataService = {
        rate: function (rating) {
            return {
                then: function (callback) {
                    setTimeout(function () {
                        callback((Math.random() * 5));
                    }, 1000);
                }
            }
        }
    }
}

function animate(all) {
    var elem = all.shift(); //Remove the top element from the array

    //animate it
    $(elem).animate({
        "opacity": 1
    }, function () {
        if (all.length > 0)
            window.setTimeout(animate(all), 700); //set the time out after the delay of 1 sec for next element to animate.
    });

}

let allDesktop = $('.header a').toArray();
animate(allDesktop);

var rates = document.getElementsByClassName("card-rate");
for (var i = 0; i < rates.length; i++) {
    let rate = rates[i].getAttribute("data-rate");
    let rateFloat = parseFloat(rate.replace(",","."))
    raterJs({
        max: 5,
        readOnly: true,
        rating: rateFloat,
        element: rates[i]
    });   

}


window.addEventListener("load", onload, false); 