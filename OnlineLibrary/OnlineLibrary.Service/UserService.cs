﻿using OnlineLibrary.Data.CRUD;
using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Service
{
    public class UserService
    {

        public  int Insert(UserDto dto)
        {
            return User.Insert(dto); //pozivanje metoda za insert knjige u data sloju
        }

        public UserDto GetByUserName(string username)
        {
            return User.GetByUserName(username);
        }

        public UserDto GetByToken(string token)
        {
            return User.GetByToken(token);
        }
    }
}
