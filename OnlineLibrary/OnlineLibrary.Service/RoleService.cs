﻿using OnlineLibrary.Data.CRUD;
using OnlineLibrary.Domain;
using OnlineLibrary.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Service
{
    public class RoleService : IRoleService
    {
        public List<RoleDto> GetAll()
        {
            return Roles.GetAll();
        }

        public List<RoleDto> GetAllByUsername(string  username)
        {
            return Roles.GetAllByUsername(username);
        }
    }
}
