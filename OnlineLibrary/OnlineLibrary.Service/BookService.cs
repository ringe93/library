﻿using OnlineLibrary.Data.Common;
using OnlineLibrary.Data.CRUD;
using OnlineLibrary.Data.Domain;
using OnlineLibrary.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Service
{
    public class BookService:IBookService
    {
        public void Delete(BookDto dto)
        {
            Book.Delete(dto);
        }

        public int Edit(BookDto dto)
        {
            return Book.Edit(dto);
        }

        public BookDto GetBookByCaption(string caption)
        {
            return Book.GetBookByCaption(caption);
        }

        public BookDto GetBookById(int id)
        {
            return Book.GetBookById(id);
        }

        public int Insert(BookDto dto)
        {
          return Book.Insert(dto); //pozivanje metoda za insert knjige u data sloju
        }

        public List<BookDto> GetAll()
        {
            return Books.GetAll();
        }

        public List<BookDto> GetTopRated(int number = Constants.Books.RatedBooks)
        {
            return Books.GetTopRated(number);
        }
    }
}
