﻿using OnlineLibrary.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Service.Interfaces
{
    public interface IRoleService
    {

        List<RoleDto> GetAll();

        List<RoleDto> GetAllByUsername(string username);

    }
}
