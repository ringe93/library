﻿using OnlineLibrary.Data.Common;
using OnlineLibrary.Data.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineLibrary.Service.Interfaces
{
    public interface IBookService
    {
        int Insert(BookDto dto);

        int Edit(BookDto dto);

        void Delete(BookDto dto);

        BookDto GetBookByCaption(string caption);

        BookDto GetBookById(int id);

        List<BookDto> GetAll();

        List<BookDto> GetTopRated(int number = Constants.Books.RatedBooks);
    }
}
